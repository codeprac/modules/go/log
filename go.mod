module gitlab.com/codeprac/modules/go/log

go 1.16

require (
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/sys v0.0.0-20210820121016-41cdb8703e55 // indirect
)
